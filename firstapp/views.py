from django.shortcuts import render
from django.http import HttpResponse
import requests

## pip install requests
def first(request):
    return render(request,"first.html")

def about(r):
    return HttpResponse("<h1 style='color:red;'>THIS IS ANOTHER DJANGO VIEW</h1>")

def index(request):
    str = "I am from views file"
    data = ["red","green","blue","magenta","black"]
    context ={"colors":data,"string":str}
    return render(request,"index.html",context)

def countries(request):
    data= requests.get("https://restcountries.eu/rest/v2/all").json()
    return render(request,"countries_data.html",{"con":data,"total":len(data)}) 



